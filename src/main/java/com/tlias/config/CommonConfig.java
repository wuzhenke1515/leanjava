/**
 * ___      ___   __     ______    __    __       __       _______  ___
 * |"  \    /"  | |" \   /" _  "\  /" |  | "\     /""\     /"     "||"  |
 * \   \  //   | ||  | (: ( \___)(:  (__)  :)   /    \   (: ______)||  |
 * /\\  \/.    | |:  |  \/ \      \/      \/   /' /\  \   \/    |  |:  |
 * |: \.        | |.  |  //  \ _   //  __  \\  //  __'  \  // ___)_  \  |___
 * |.  \    /:  | /\  |\(:   _) \ (:  (  )  :)/   /  \\  \(:      "|( \_|:  \
 * |___|\__/|___|(__\_|_)\_______) \__|  |__/(___/    \___)\_______) \_______)
 * <p>
 * User: michaelwu
 * Date: 2023/4/1
 * Time: 11:53
 */
package com.tlias.config;

import org.dom4j.io.SAXReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 通用配置类
 * @author michaelwu
 * @date 2023-04-01  11:53
 *
 **/
@Configuration  //配置类
public class CommonConfig {
    //声明第三方bean
    /*
     * 如果要管理bean 对象来自于第三方（不是自定义的）， 是无法用@Component 及衍生注解声明bean 的，就需要用到@Bean注解。
     * 若要管理的第三方bean对象，建议对这些bean进行集中分类配置，可以通过@Configuration注解声明一个配置类
     *不建议在启动类里添加
     * 通过@Bean注解的name/value属性指定bean名称，如果未指定，默认是方法名
     * 如果第三方bean 需要依赖其它bean 对象，直接在bean 定义予以罚款设置形参即可，容器会根据类型自动装配
     * 项目中自定义的，使用@Component 及其衍生注解
     * 项目中引入第三方的，使用@Bean 注解
     * */
    @Bean  //将当前方法的返回值对象交给IOC容器管理，成为IOC容器bean对象
    public SAXReader saxReader() {
        return new SAXReader();  //不建议
    }
}
