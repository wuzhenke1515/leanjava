/**
 * ___      ___   __     ______    __    __       __       _______  ___
 * |"  \    /"  | |" \   /" _  "\  /" |  | "\     /""\     /"     "||"  |
 * \   \  //   | ||  | (: ( \___)(:  (__)  :)   /    \   (: ______)||  |
 * /\\  \/.    | |:  |  \/ \      \/      \/   /' /\  \   \/    |  |:  |
 * |: \.        | |.  |  //  \ _   //  __  \\  //  __'  \  // ___)_  \  |___
 * |.  \    /:  | /\  |\(:   _) \ (:  (  )  :)/   /  \\  \(:      "|( \_|:  \
 * |___|\__/|___|(__\_|_)\_______) \__|  |__/(___/    \___)\_______) \_______)
 * <p>
 * User: michaelwu
 * Date: 2023/3/31
 * Time: 17:24
 */
package com.tlias.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 *
 * @author michaelwu
 * @date 2023-03-31  17:24
 *
 **/
@Aspect
@Component
@Slf4j
public class MyAspect {
    @Pointcut("execution(* com.tlias.service.*.*(..))")
    public void pt(){}

    @Around("pt()")
    public Object record(ProceedingJoinPoint joinPoint) throws Throwable {

        //获取目标对象的类名
        String className = joinPoint.getTarget().getClass().getName();
        log.info("获取目标对象的类名：{}", className);

        //获取目标对象的方法名
        String methondName = joinPoint.getSignature().getName();
        log.info("获取目标方法的方法名：{}", methondName);

        //获取目标方法运行时传入的参数
        Object[] args = joinPoint.getArgs();
        log.info("获取目标方法的参数：{}", Arrays.toString(args));

        Object proceed = joinPoint.proceed();


        //获取目标方法运行后返回的结果
        log.info("目标方法运行时返回的结果：{}", proceed);


        return proceed;
    }

    @Before("pt()")
    public void before(JoinPoint joinPoint) {

        log.info("目标方法执行前操作。。。。。");
    }
}
