/**
 * ___      ___   __     ______    __    __       __       _______  ___
 * |"  \    /"  | |" \   /" _  "\  /" |  | "\     /""\     /"     "||"  |
 * \   \  //   | ||  | (: ( \___)(:  (__)  :)   /    \   (: ______)||  |
 * /\\  \/.    | |:  |  \/ \      \/      \/   /' /\  \   \/    |  |:  |
 * |: \.        | |.  |  //  \ _   //  __  \\  //  __'  \  // ___)_  \  |___
 * |.  \    /:  | /\  |\(:   _) \ (:  (  )  :)/   /  \\  \(:      "|( \_|:  \
 * |___|\__/|___|(__\_|_)\_______) \__|  |__/(___/    \___)\_______) \_______)
 * <p>
 * User: michaelwu
 * Date: 2023/3/31
 * Time: 9:05
 */
package com.tlias.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 *
 * @author michaelwu
 * @date 2023-03-31  09:05
 *
 **/
@Slf4j
@Component
@Aspect  //注释此注解，则此切面类将失效
public class TimeAspect {

    // @Pointcut("execution(* com.tlias.service.*.*(..))")  //切入点
    // @Pointcut("execution(* com.tlias.service.*.*(..)) || execution(* com.tlias.service.*.*(..))")  //切入点
    // @Pointcut("execution(public void com.tlias.service.impl.DeptServiceImpl.getById(java.lang.Integer))")  //切入点
    //execution(访问修饰符？ 返回值  包名.类名.?方法名（参数）  throw 异常？)
    /*
    *  两种通配符   *  .
    * execution(public void com.tlias.service.impl.DeptServiceImpl.getById(java.lang.Integer))
    * execution(public void com.tlias.service.impl.DeptServiceImpl.getById(..)) 任意类型或个数参数
    * execution(public void com..service.impl.DeptServiceImpl.getById(..)) 任意包
    * execution(public * *(..)) 此包下所有方法
    * 访问修改符： 可省略 （比如 public protected）
    * 包名。类名：可省略
    * throw异常：可省略（注意是方法上声明抛出的异常，不是实际抛出的异常）
    * */
    @Pointcut("@annotation(com.tlias.aop.AspectAnnotation)")
    public void pt(){}
    @Around("pt()") //切入点表达时
    // @Around("execution(* com.tlias.service.*.*(..))") //切入点表达时
    // @Before()
    // @After()
    // @AfterReturning();
    // @AfterThrowing
    // @Order(2) 执行切面类的执行顺序 数字越小，越先执行， 默认与类名相关
    //Around 使用ProceedingJoinPoint  ,其他使用JoinPoint
    public Object recordTime(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {


        //1。记录开始时间
        long start = System.currentTimeMillis();

        //2。调用原始方法运行
        Object proceed = proceedingJoinPoint.proceed();

        // 3。记录结束时间，计算方法执行的耗时

        long end = System.currentTimeMillis();
        //使用日志记录方法执行耗时
        log.info(proceedingJoinPoint.getSignature() + "方法执行耗时：{}ms", end-start);
        //返回原始方法执行后的结果

        return proceed;


    }
}
