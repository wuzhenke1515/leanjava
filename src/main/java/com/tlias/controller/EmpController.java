/**
 * ___      ___   __     ______    __    __       __       _______  ___
 * |"  \    /"  | |" \   /" _  "\  /" |  | "\     /""\     /"     "||"  |
 * \   \  //   | ||  | (: ( \___)(:  (__)  :)   /    \   (: ______)||  |
 * /\\  \/.    | |:  |  \/ \      \/      \/   /' /\  \   \/    |  |:  |
 * |: \.        | |.  |  //  \ _   //  __  \\  //  __'  \  // ___)_  \  |___
 * |.  \    /:  | /\  |\(:   _) \ (:  (  )  :)/   /  \\  \(:      "|( \_|:  \
 * |___|\__/|___|(__\_|_)\_______) \__|  |__/(___/    \___)\_______) \_______)
 * <p>
 * User: michaelwu
 * Date: 2023/3/29
 * Time: 16:27
 */
package com.tlias.controller;

import com.tlias.pojo.Emp;
import com.tlias.pojo.Result;
import com.tlias.service.EmpService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *
 * @author michaelwu
 * @date 2023-03-29  16:27
 *
 **/
@RestController
@RequestMapping("emp")
@Slf4j

public class EmpController {

    @Autowired
    private EmpService empService;

    @GetMapping()
    public Result empList() {
        List<Emp> empList = empService.getEmpListService();
        log.info("查询所有雇员信息");
        return Result.success(empList);
    }

    @GetMapping("/{id}")
    public Result getEmp(@PathVariable int id){
        Emp byid = empService.getByidSerice(id);
        log.info("根据id查询emp数据");
        return Result.success(byid);
    }

    @GetMapping("/empanddept/{id}")
    public Result getEmpAndDept(@PathVariable int id) {
        Emp empAndDept = empService.getEmpAndDept(id);
        System.out.println(empAndDept);
        log.info("关联出部门名称：{}", empAndDept);
        return Result.success(empAndDept);
    }

    @GetMapping("/thirdEmp/{id}")
    public Result EmpAndDeptByStepThree(@PathVariable("id") int id) {
        Emp emp = empService.EmpAndDeptBystepThird(id);
        log.info("第三种查询关联数据：{}", emp);
        return Result.success(emp);
    }

}
