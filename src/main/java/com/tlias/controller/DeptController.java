/**
 * ___      ___   __     ______    __    __       __       _______  ___
 * |"  \    /"  | |" \   /" _  "\  /" |  | "\     /""\     /"     "||"  |
 * \   \  //   | ||  | (: ( \___)(:  (__)  :)   /    \   (: ______)||  |
 * /\\  \/.    | |:  |  \/ \      \/      \/   /' /\  \   \/    |  |:  |
 * |: \.        | |.  |  //  \ _   //  __  \\  //  __'  \  // ___)_  \  |___
 * |.  \    /:  | /\  |\(:   _) \ (:  (  )  :)/   /  \\  \(:      "|( \_|:  \
 * |___|\__/|___|(__\_|_)\_______) \__|  |__/(___/    \___)\_______) \_______)
 * <p>
 * User: michaelwu
 * Date: 2023/3/28
 * Time: 13:55
 */
package com.tlias.controller;

import com.tlias.pojo.Dept;
import com.tlias.pojo.Emp;
import com.tlias.pojo.Result;
import com.tlias.service.DeptService;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 *
 * @author michaelwu
 * @date 2023-03-28  13:55
 *
 **/
@Slf4j
@RestController
@RequestMapping("dept")
// @Lazy  //延迟初始化
@Scope("singleton")  //配置bean的作用域
/*
* 默认singleton 的 bean 在容器启动时被创建，可以使用@Lazy注解来延迟初始化（延迟到第一次使用时）、
* prototype 的 bean ,每一次使用该bean 的时候都会创建一个新的实例
* 实际开发当中，绝大多数的Bean 是单例的，也就是说绝大部分Bean 不需要配置scope属性
* */
public class DeptController {

    public DeptController() {
       log.info("qwqwq");
    }

    @Autowired
    private DeptService deptService;
    // @RequestMapping(value = "dept", method = RequestMethod.GET)
    @GetMapping
    public Result list() {
        List<Dept> deptList = deptService.list();
        log.info("查询全部部门接口");
        return Result.success(deptList);
    }

    @GetMapping("/alldept")
    public Result allDept() {
       List<Dept> deptList = deptService.allDept();
       log.info("通过另一种方法查询全部");
       return  Result.success(deptList);
    }

    @GetMapping("/{id}")
    public Result getDept(@PathVariable Integer id) {
        Dept dept = deptService.getByIdService(id);
        return Result.success(dept);
    }

    @GetMapping("/mapdept")
    Result deptMap() {
       Map<String, Object> deptMap = deptService.deptServiceMap();
        log.info("this{}",deptMap);
       return Result.success(deptMap);
    }

    @GetMapping("/deptrelation/{id}")
    Result deptRelation (@PathVariable("id") int id) {
        Dept dept = deptService.deptRelation(id);

        log.info("this{}",dept);
        return Result.success(dept);

    }
}
