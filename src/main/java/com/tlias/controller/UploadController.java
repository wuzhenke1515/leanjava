/**
 * ___      ___   __     ______    __    __       __       _______  ___
 * |"  \    /"  | |" \   /" _  "\  /" |  | "\     /""\     /"     "||"  |
 * \   \  //   | ||  | (: ( \___)(:  (__)  :)   /    \   (: ______)||  |
 * /\\  \/.    | |:  |  \/ \      \/      \/   /' /\  \   \/    |  |:  |
 * |: \.        | |.  |  //  \ _   //  __  \\  //  __'  \  // ___)_  \  |___
 * |.  \    /:  | /\  |\(:   _) \ (:  (  )  :)/   /  \\  \(:      "|( \_|:  \
 * |___|\__/|___|(__\_|_)\_______) \__|  |__/(___/    \___)\_______) \_______)
 * <p>
 * User: michaelwu
 * Date: 2023/4/1
 * Time: 17:22
 */
package com.tlias.controller;

import com.aliyun.oss.AliOSSUtils;
import com.tlias.pojo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


/**
 * 文件上传
 * @author michaelwu
 * @date 2023-04-01  17:22
 *
 **/
@Component
@RestController
@Slf4j
public class UploadController {
    @Autowired
    private AliOSSUtils aliOSSUtils;

    //本地存储
    // @PostMapping("/upload")
    // public Result upload(String name, Integer age, MultipartFile image) throws IOException {
    //
    //     log.info("文件上传：{}，{}，{}", name, age,image);
    //     String originalFilename = image.getOriginalFilename();
    //     int index = originalFilename.lastIndexOf(".");
    //     String extname = originalFilename.substring(index);
    //     String filename = UUID.randomUUID().toString() + extname;
    //     log.info("新文件名称：{}", filename);
    //     image.transferTo(new File("E:\\images_oss\\" + filename));
    //     return Result.success();
    // }

    @PostMapping("/upload")
    public Result upload(String name, Integer age, MultipartFile image) throws IOException {

        log.info("文件上传：{}，{}，{}", name, age,image);
        String originalFilename = image.getOriginalFilename();
        String upload = aliOSSUtils.upload(image);
        // int index = originalFilename.lastIndexOf(".");
        // String extname = originalFilename.substring(index);
        // String filename = UUID.randomUUID().toString() + extname;
        // log.info("新文件名称：{}", filename);
        // image.transferTo(new File("E:\\images_oss\\" + filename));
        return Result.success(upload);
    }
}
