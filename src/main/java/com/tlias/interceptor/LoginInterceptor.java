/**
 * ___      ___   __     ______    __    __       __       _______  ___
 * |"  \    /"  | |" \   /" _  "\  /" |  | "\     /""\     /"     "||"  |
 * \   \  //   | ||  | (: ( \___)(:  (__)  :)   /    \   (: ______)||  |
 * /\\  \/.    | |:  |  \/ \      \/      \/   /' /\  \   \/    |  |:  |
 * |: \.        | |.  |  //  \ _   //  __  \\  //  __'  \  // ___)_  \  |___
 * |.  \    /:  | /\  |\(:   _) \ (:  (  )  :)/   /  \\  \(:      "|( \_|:  \
 * |___|\__/|___|(__\_|_)\_______) \__|  |__/(___/    \___)\_______) \_______)
 * <p>
 * User: michaelwu
 * Date: 2023/3/30
 * Time: 15:49
 */
package com.tlias.interceptor;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author michaelwu
 * @date 2023-03-30  15:49
 *
 **/
@Component
public class LoginInterceptor implements HandlerInterceptor {
    @Override //目前资源执行前运行，返回true  放行， 返回false 不放行
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        return HandlerInterceptor.super.preHandle(request, response, handler);
    }

    @Override  //目标资源方法运行后运行
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    @Override //视图渲染完毕后运行，最后运行
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}
