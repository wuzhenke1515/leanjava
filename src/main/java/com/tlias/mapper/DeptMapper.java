/**
 * ___      ___   __     ______    __    __       __       _______  ___
 * |"  \    /"  | |" \   /" _  "\  /" |  | "\     /""\     /"     "||"  |
 * \   \  //   | ||  | (: ( \___)(:  (__)  :)   /    \   (: ______)||  |
 * /\\  \/.    | |:  |  \/ \      \/      \/   /' /\  \   \/    |  |:  |
 * |: \.        | |.  |  //  \ _   //  __  \\  //  __'  \  // ___)_  \  |___
 * |.  \    /:  | /\  |\(:   _) \ (:  (  )  :)/   /  \\  \(:      "|( \_|:  \
 * |___|\__/|___|(__\_|_)\_______) \__|  |__/(___/    \___)\_______) \_______)
 * <p>
 * User: michaelwu
 * Date: 2023/3/28
 * Time: 14:09
 */
package com.tlias.mapper;

import com.tlias.pojo.Dept;
import com.tlias.pojo.Emp;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 *
 * @author michaelwu
 * @date 2023-03-28  14:09
 *
 **/
@Mapper
public interface DeptMapper {

    @Select("select  * from dept")
    public List<Dept> list();


    @Select("select * from dept")
    public List<Dept> selectAll();


    Dept getById(Integer id);


    //此用法只能在xml文件中写查询语句   List<Map<String, Object>> deptMap();
    @MapKey("id")
    // @Select("select * from dept")
    // List<Map<String, Object>> deptMap();
    // Map<String, Emp> deptMap();
    Map<String, Object> deptMap();

    Dept assDeptMap(Integer id);

    Dept deptRelationlist(int id);
}
