/**
 * ___      ___   __     ______    __    __       __       _______  ___
 * |"  \    /"  | |" \   /" _  "\  /" |  | "\     /""\     /"     "||"  |
 * \   \  //   | ||  | (: ( \___)(:  (__)  :)   /    \   (: ______)||  |
 * /\\  \/.    | |:  |  \/ \      \/      \/   /' /\  \   \/    |  |:  |
 * |: \.        | |.  |  //  \ _   //  __  \\  //  __'  \  // ___)_  \  |___
 * |.  \    /:  | /\  |\(:   _) \ (:  (  )  :)/   /  \\  \(:      "|( \_|:  \
 * |___|\__/|___|(__\_|_)\_______) \__|  |__/(___/    \___)\_______) \_______)
 * <p>
 * User: michaelwu
 * Date: 2023/3/28
 * Time: 14:09
 */
package com.tlias.mapper;

import com.tlias.pojo.Emp;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 *
 * @author michaelwu
 * @date 2023-03-28  14:09
 *
 **/
@Mapper
public interface EmpMapper {
    List<Emp> getEmpList();

    @Select("select  * from emp where id = #{id}")
    Emp getByid(int id);

    Emp getEmpAndDept(int id);

    // Emp empAndDeptByStepThree(int id);

    List<Emp> getEmpByStep(int id);

    /*第一种批量删除*/
    Integer deleteMoreByArray(Integer[] ids);

    /**
     * 第二种批量删除
     * @param ids
     * @return
     */
    Integer deleteMoreByArrayTwo(Integer[] ids);

    int insertMoreByList(@Param("emps") List<Emp> emps);

    Emp EmpAndDeptByStepThree(int id);
}
