package com.tlias.service;

import com.tlias.pojo.Dept;
import com.tlias.pojo.Emp;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * ___      ___   __     ______    __    __       __       _______  ___
 * |"  \    /"  | |" \   /" _  "\  /" |  | "\     /""\     /"     "||"  |
 * \   \  //   | ||  | (: ( \___)(:  (__)  :)   /    \   (: ______)||  |
 * /\\  \/.    | |:  |  \/ \      \/      \/   /' /\  \   \/    |  |:  |
 * |: \.        | |.  |  //  \ _   //  __  \\  //  __'  \  // ___)_  \  |___
 * |.  \    /:  | /\  |\(:   _) \ (:  (  )  :)/   /  \\  \(:      "|( \_|:  \
 * |___|\__/|___|(__\_|_)\_______) \__|  |__/(___/    \___)\_______) \_______)
 * <p>
 * User: michaelwu
 * Date: 2023/3/28
 * Time: 13:59
 */
@Service
public interface DeptService {
    List<Dept> list();

    List<Dept> allDept();

    Dept getByIdService(Integer id);

    // Map<String, Emp> deptServiceMap();
    Map<String, Object> deptServiceMap();

    Dept deptRelation(int id);
}
