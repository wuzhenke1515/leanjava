/**
 * ___      ___   __     ______    __    __       __       _______  ___
 * |"  \    /"  | |" \   /" _  "\  /" |  | "\     /""\     /"     "||"  |
 * \   \  //   | ||  | (: ( \___)(:  (__)  :)   /    \   (: ______)||  |
 * /\\  \/.    | |:  |  \/ \      \/      \/   /' /\  \   \/    |  |:  |
 * |: \.        | |.  |  //  \ _   //  __  \\  //  __'  \  // ___)_  \  |___
 * |.  \    /:  | /\  |\(:   _) \ (:  (  )  :)/   /  \\  \(:      "|( \_|:  \
 * |___|\__/|___|(__\_|_)\_______) \__|  |__/(___/    \___)\_______) \_______)
 * <p>
 * User: michaelwu
 * Date: 2023/3/28
 * Time: 14:02
 */
package com.tlias.service.impl;

import com.tlias.aop.AspectAnnotation;
import com.tlias.mapper.DeptMapper;
import com.tlias.pojo.Dept;
import com.tlias.pojo.Emp;
import com.tlias.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 *
 * @author michaelwu
 * @date 2023-03-28  14:02
 *
 **/
@Service
public class DeptServiceImpl implements DeptService {

   @Autowired
    private DeptMapper deptMapper;

    @Override
    public List<Dept> list() {
        return deptMapper.list();
    }

    @Override
    public List<Dept> allDept() {
        return deptMapper.selectAll();
    }

    @AspectAnnotation
    @Override
    public Dept getByIdService(Integer id) {
        return deptMapper.getById(id);
    }

    @Override
    // public Map<String, Emp> deptServiceMap() {
    public Map<String, Object> deptServiceMap() {
        return deptMapper.deptMap();
    }

    @Override
    public Dept deptRelation(int id) {
        return deptMapper.deptRelationlist(id);
    }
}
