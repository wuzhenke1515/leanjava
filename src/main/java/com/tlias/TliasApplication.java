package com.tlias;

import org.dom4j.io.SAXReader;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@ServletComponentScan
//开启定时任务功能
@EnableScheduling
// @MapperScan("com.tlias.mapper")
public class TliasApplication {

    public static void main(String[] args) {
        SpringApplication.run(TliasApplication.class, args);
    }

    //声明第三方bean
/*
* 如果要管理bean 对象来自于第三方（不是自定义的）， 是无法用@Component 及衍生注解声明bean 的，就需要用到@Bean注解。
* 若要管理的第三方bean对象，建议对这些bean进行集中分类配置，可以通过@Configuration注解声明一个配置类
*不建议在启动类里添加
* */
    // @Bean  //将当前方法的返回值对象交给IOC容器管理，成为IOC容器bean对象
    // public SAXReader saxReader() {
    //     return new SAXReader();  //不建议
    // }


    




}
