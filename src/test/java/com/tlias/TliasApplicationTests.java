package com.tlias;

import com.tlias.controller.DeptController;
import com.tlias.mapper.DeptMapper;
import com.tlias.mapper.EmpMapper;
import com.tlias.pojo.Dept;
import com.tlias.pojo.Emp;
import com.tlias.service.SendMailService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import jakarta.mail.MessagingException;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.*;

@SpringBootTest
class TliasApplicationTests {

    @Autowired
    private ApplicationContext applicationContext;// IOC 容器对象

    @Autowired
    private SendMailService sendMailService;

    @Autowired
    private SAXReader saxReader;   //将第三方bean交给容器
    @Autowired
    private DeptMapper         deptMapper;

    @Autowired
    private EmpMapper empMapper;

    @Test
    void testSendMail() {
        sendMailService.sendMail();
    }

    @Test
    void testSendMimMail() {
        sendMailService.sendMimeMail();
    }

    @Test
    void testSendFuJianMail() throws MessagingException {
        sendMailService.sendPakageMail();
    }


    @Test
    void selectAllDept() {
        deptMapper.selectAll();
        Assertions.assertDoesNotThrow(this::selectAllDept);

    }

    @Test
    void testJwt() {
        Map<String, Object> claims = new HashMap<>();
        claims.put("id", 1);
        claims.put("name", "Tom");

        String itheima = Jwts.builder()
                .signWith(SignatureAlgorithm.HS256, "itheima") // 签名算法
                .addClaims(claims) // 设置自定义内容（载荷）
                .setExpiration(new Date(System.currentTimeMillis() + 3600 * 1000)) // 设置有效时间
                .compact();
        System.out.println(itheima);
    }

    // @Test
    // public void testEntryJwt() {
    //     Claims itheima = Jwts.parser()
    //             .setSigningKey("itheima")
    //             .parseClaimsJws("eyJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiVG9tIiwiaWQiOjEsImV4cCI6MTY4MDE2MTA5MX0.osaFRgjWSs7wtvPCPYIPcPcOIvHRMOSBGxecSm5RS0g")
    //             .getBody();
    //     System.out.println(itheima);
    // }

    @Test
    void testGetBean() {

        DeptController bean1 = applicationContext.getBean(DeptController.class);
        System.out.println(bean1);

        DeptController bean2 = (DeptController) applicationContext.getBean("deptController");


        DeptController bean3 = applicationContext.getBean("deptController", DeptController.class);
        System.out.println(bean3);
    }


    /**
     * 第三方bean
     *
     */
    @Test
     void testThirdBean() throws DocumentException {
        Document read = saxReader.read("E:\\java_code\\tlias\\src\\main\\resources\\mapper\\DeptMapper.xml");
        Element rootElement = read.getRootElement();
        System.out.println(rootElement.toString());
    }

    @Test
     void testCollerctMap() {
        Dept dept = deptMapper.deptRelationlist(2);
        System.out.println(dept);
    }

    @Test
     void testDeleteMore() {
        empMapper.deleteMoreByArray(new Integer[]{4});
    }

    @Test
     void testInsertMoreByList(){

        Emp emp1 = new Emp(0,"张忆","无忌",1, "1.jpg",  1, LocalDate.now(), 2, LocalDateTime.now(), LocalDateTime.now());
       // Emp emp1 = new Emp(0,"张ag忌", "","无忌",1, "1.jpg",  1, LocalDate.now(), 2, LocalDateTime.now(), LocalDateTime.now(),null);
       // Emp emp2 = new Emp(0, "张五丰","", "三丰",1, "1.jpg",  2, LocalDate.now(), 2, LocalDateTime.now(), LocalDateTime.now() ,null);
       // Emp emp3 = new Emp(0, "张五山","", "翠山", 1, "1.jpg",  3, LocalDate.now(), 2, LocalDateTime.now(), LocalDateTime.now() ,null);
        List<Emp> emps = Arrays.asList(emp1);
        // List<Emp> emps = Arrays.asList(emp1,emp2,emp3);
        int insertMoreByList = empMapper.insertMoreByList(emps);
        System.out.println(insertMoreByList);

    }
    @Test
     void testDate(){

        System.out.println(LocalDate.now());
    }

}
